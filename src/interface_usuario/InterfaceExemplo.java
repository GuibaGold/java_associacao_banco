package interface_usuario;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class InterfaceExemplo {
	
	private JFrame mainFrame;
	private JLabel headerLabel;
	private JPanel controlPanel;
	private JLabel statusLabel;
	
	public InterfaceExemplo() {
		preparaGUI();
		criaPainel();
	}
	
	private void preparaGUI() {
		mainFrame = new JFrame("Nome do Programa");
		mainFrame.setSize(400, 500);
		mainFrame.setLocation(3000,200);
		mainFrame.setLayout(new GridLayout(3,1));
		
		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0); 
			}
		});
		
		headerLabel = new JLabel("T�tulo", JLabel.CENTER);
		
		statusLabel = new JLabel("Estado", JLabel.CENTER);
		statusLabel.setSize(350, 100);
		
		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout());
		
		mainFrame.add(headerLabel);
		mainFrame.add(controlPanel);
		mainFrame.add(statusLabel);
		
		mainFrame.setVisible(true);
	}
	
	private void criaPainel() {
		headerLabel.setText("Clique em um dos bot�es");
		JButton okButton = new JButton("OK");
		JButton submeterButton = new JButton("Submeter");
		JButton cancelarButton = new JButton("Cancelar");
		
		okButton.setActionCommand("Ok");
		submeterButton.setActionCommand("Submeter");
		cancelarButton.setActionCommand("Cancelar");
		
		okButton.addActionListener(new ButtonClickListener());
		submeterButton.addActionListener(new ButtonClickListener());
		cancelarButton.addActionListener(new ButtonClickListener());
		
		controlPanel.add(okButton);
		controlPanel.add(submeterButton);
		controlPanel.add(cancelarButton);
		
	}
	
	private class ButtonClickListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			String comando = e.getActionCommand();
			statusLabel.setText("Bot�o" + comando + " apertado!");
		}
	}
	public static void main(String args[]) {
		InterfaceExemplo tela = new InterfaceExemplo();
	}
}

